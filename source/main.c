#include <stdio.h>
#include "dice.h"

/* This program implements a virtual die */
/* comment pg 21 of "Semana_2___Git___Parte_I.pdf" - remote repository */

int n;

int main() {
    initializeSeed();
    printf("Number of die faces: ");
    scanf("%d", &n);
    printf("Let's roll the dice: %d\n", rollDice(n));
    return 0;
}
