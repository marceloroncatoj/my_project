#include "dice.h"

void initializeSeed(){
    srand(time(NULL));
}

int rollDice(int n){
    return (rand() % n) + 1;
}
